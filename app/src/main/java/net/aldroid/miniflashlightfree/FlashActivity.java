package net.aldroid.miniflashlightfree;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;

import java.io.IOException;
import java.util.List;

public class FlashActivity extends Activity {
    private Object camera;
    private int torchType = 0;
    // 0 - show only main torch
    // 1 - show main and screen
    // 2 - show all torches

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toggleFlashLight(true);
    }

    public void toggleFlashLight(boolean enabled){
        try {
            torchType = getPreferences(MODE_PRIVATE).getInt("t", 0);
            getWindow().getAttributes().screenBrightness = torchType >= 1 ? 1f : -1f;
            getWindow().setAttributes(getWindow().getAttributes());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CameraManager cameraManager = (CameraManager) getApplicationContext().getSystemService(Context.CAMERA_SERVICE);
                if (cameraManager == null) return;
                for (String id : cameraManager.getCameraIdList()) {
                    Boolean isFlashAvailable = cameraManager.getCameraCharacteristics(id).get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                    if (isFlashAvailable != null && isFlashAvailable) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            cameraManager.setTorchMode(id, enabled);
                        }
                        else
                            toggleFlashLightOldApi(enabled);
                    }
                    if (torchType < 2) enabled = false;
                }
            }
            else {
                toggleFlashLightOldApi(enabled);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP)
        {
            SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
            torchType++;
            if (torchType > 2) torchType = 0;
            editor.putInt("t", torchType);
            editor.apply();
            toggleFlashLight(true);
        }

        return super.dispatchTouchEvent(ev);
    }

    private void toggleFlashLightOldApi(boolean enabled) {
        if (camera == null) camera = Camera.open();
        Camera mCam = (Camera) camera;
        Camera.Parameters p = mCam.getParameters();
        List<String> modes = p.getSupportedFlashModes();
        boolean torch = modes != null && modes.contains(Camera.Parameters.FLASH_MODE_TORCH);
        p.setFlashMode(enabled ? torch ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_ON : Camera.Parameters.FLASH_MODE_OFF);
        mCam.setParameters(p);

        if (enabled) {
            SurfaceTexture mPreviewTexture = new SurfaceTexture(0);
            try {
                mCam.setPreviewTexture(mPreviewTexture);
            } catch (IOException ex) {
                // Ignore
            }
            mCam.startPreview();
        }
        else mCam.stopPreview();
    }

    @Override
    public void onBackPressed() {
        toggleFlashLight(false);
        super.onBackPressed();
    }

//
//    @Override
//    protected void onPause() {
//        toggleFlashLight(false);
//        super.onPause();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        toggleFlashLight(true);
    }
}
